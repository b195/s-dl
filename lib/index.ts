import * as base62 from 'base-62.js';
import { spawn } from 'child_process';

import CDM from './cdm';
import {
  SpotifyAlbumExtended,
  SpotifyPlaylist,
  SpotifyTrack,
  SpotifyTrackExtended
} from './types';

type CookiesType = 'header' | 'netscape';
type Cookies = Buffer | string;
type CookiesRotationFunction = () => Cookies | Promise<Cookies>;
interface SpotifyDLCookiesOptions {
  cookies?: Cookies;
  rotation?: CookiesRotationFunction;
  type: CookiesType;
};

interface SpotifyDLOptions {
  clientId: Buffer;
  cookies: SpotifyDLCookiesOptions;
  premium?: boolean;
  privateKey: Buffer;
};

interface SpotifyDLDownloadOptions {
  premium?: boolean;
  spawn?: boolean;
  timeout?: number;
};

enum ResponseType {
  JSON = 'json',
  BUFFER = 'buffer',
  TEXT = 'text'
};

export * from './types';

const UPDATE_AUTH_TOKEN_EVERY_MS = 60 * 60 * 1000;
const FFMPEG_PADDING_TIMEOUT_MS = 5000;

function formCookieHeader(cookies: Buffer | string) {
  if (cookies instanceof Buffer)
    cookies = cookies.toString();

  let result = '';

  for (const line of cookies.split('\n')) {
    if (line.trim() === '' || line.startsWith('#'))
      continue;

    const [ domain, _include, path, secure, _expiry, key, value ] = line.split('\t');
    if (!(domain.startsWith('.') || domain.startsWith('www')) || secure.toLowerCase() !== 'true' ||
      path !== '/')
      continue;
    result += `${key}=${value};`;
  }

  return result;
}

const ACCESS_TOKEN_REGEX = /accessToken":"(.*?)"/g;

export default class SpotifyDL {
  private accessToken?: string;
  private accessTokenUpdateInProgress = false;
  private accessTokenPromises: (() => void)[][] = [];
  private cdm: CDM;
  private cookies: SpotifyDLCookiesOptions;
  private cookie?: string;
  private lastUpdated: number = 0;
  private premium = false;

  constructor(options: SpotifyDLOptions) {
    this.cookies = options.cookies;

    this.cdm = new CDM(options.privateKey, options.clientId);
    this.premium = options.premium || this.premium;
    this.refreshToken();
  }

  private async refreshToken(force = false) {
    if (this.accessTokenUpdateInProgress)
      return await new Promise<void>((res, rej) =>
        this.accessTokenPromises.push([res, rej])
      );
    if (Date.now() - this.lastUpdated < UPDATE_AUTH_TOKEN_EVERY_MS && !force)
      return;

    const set = (cookie: Cookies) => {
      switch (this.cookies.type) {
        case 'netscape':
          this.cookie = formCookieHeader(cookie);
          break;
        case 'header':
        default:
          this.cookie = cookie.toString();
      }
    };

    // this is valid javascript, typescript is just stubborn
    switch (true) {
      case !!this.cookies.rotation:
        const rotate = this.cookies.rotation as CookiesRotationFunction;
        set(await rotate());
        break;
      case !!this.cookies.cookies:
        set(this.cookies.cookies as Cookies);
        break;
      default:
        throw new Error("no rotation function nor cookies field given in options, can't continue");
    }

    try {
      this.accessTokenUpdateInProgress = true;
      this.accessToken = undefined;
      const data = await this.fetchWrapper<string>('get', 'https://open.spotify.com/', undefined, ResponseType.TEXT, true);

      const matches = [...data.matchAll(ACCESS_TOKEN_REGEX)][0];
      if (!matches || !matches[1])
        throw new Error('access token not found');

      this.accessToken = matches[1];

      this.lastUpdated = Date.now();
      this.accessTokenPromises.forEach(([res]) => res());
    } catch (err) {
      this.accessTokenPromises.forEach(([_, rej]) => rej());
    }

    this.accessTokenUpdateInProgress = false;
    this.accessTokenPromises = [];
  }

  private async fetchWrapper<T = any>(method: string, url: string, body?: any, responseType?: ResponseType, bailout = false): Promise<T> {
    if (!bailout)
      await this.refreshToken();

    try {
      const headers: Record<string, string> = {
        'app-platform': 'WebPlayer',
        accept: 'application/json',
        'accept-language': 'en-US',
        'content-type': 'application/json',
        origin: 'https://open.spotify.com/',
        referer: 'https://open.spotify.com/',
        'sec-ch-ua': '"Not)A;Brand";v="99", "Google Chrome";v="127", "Chromium";v="127"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-site',
        'spotify-app-version': '1.2.57.55.g3e99b4cd',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36',
      }

      if (this.accessToken)
        headers.authorization = 'Bearer ' + this.accessToken;

      if (this.cookie)
        headers.cookie = this.cookie;

      const response = await fetch(url, {
        method,
        body,
        headers
      });

      switch (responseType) {
        case ResponseType.BUFFER:
          return Buffer.from(await response.arrayBuffer()) as T;
        case ResponseType.TEXT:
          return await response.text() as T;
        case ResponseType.JSON:
        default:
          return await response.json();
      }
    } catch (err) {
      if (bailout)
        throw err;
      else {
        await this.refreshToken(true);
        return await this.fetchWrapper<T>(method, url, body, responseType, true);
      }
    }
  }

  /**
   * downloads the specified track. (episodes cannot be downloaded yet)
   *
   * @param url an open.spotify.com URL of a track, a SpotifyTrackExtended
   * object or a SpotifyTrack object.
   *
   * @param options options, containing timeout duration (0 for no timeout),
   * a "premium" flag and a flag for whether it should spawn an ffmpeg process
   * or not.
   *
   * @returns a Promise, which may resolve to an object, containing the
   * Readable stream and the information about the requested song.
   */
  public async downloadTrack(url: string | SpotifyTrackExtended | SpotifyTrack, options: SpotifyDLDownloadOptions = {}) {
    if (typeof url === 'string') {
      const URLObject = new URL(url);
      if (URLObject.host !== 'open.spotify.com')
        throw new Error('invalid URL was given, must be an open.spotify.com link');

      const parts = URLObject.pathname.split('/');
      if (!parts.includes('track'))
        throw new Error('not a track');

      url = await this.getTrack(parts.pop() as string);
    }

    if (options.timeout === undefined)
      options.timeout = url.duration_ms + FFMPEG_PADDING_TIMEOUT_MS;

    if (options.premium === undefined)
      options.premium = this.premium;

    if (options.spawn === undefined)
      options.spawn = true;

    const gid = base62.decodeHex(url.id).padStart(32, '0');
    const metadata = await this.fetchWrapper(
      'get',
      `https://spclient.wg.spotify.com/metadata/4/track/${gid}?market=from_token`
    );

    let files = metadata.file;
    if (!files) {
      if (metadata.alternative)
        files = metadata.alternative[0].file;
      else
        throw new Error('download unavailable');
    }

    const quality = options.premium ? 'MP4_256' : 'MP4_128';
    const file = files.filter((x: { format: string }) => x.format == quality)[0].file_id;

    const seektableResponse = await this.fetchWrapper('get', `https://seektables.scdn.co/seektable/${file}.json`, null, ResponseType.JSON);
    const pssh = seektableResponse.pssh;
    const challenge = this.cdm.getLicenseChallenge(pssh);

    const license = await this.fetchWrapper<Buffer>(
      'post',
      'https://gue1-spclient.spotify.com/widevine-license/v1/audio/license',
      challenge,
      ResponseType.BUFFER
    );

    const key = this.cdm.parseLicense(license);

    const streamUrl = (
      await this.fetchWrapper(
        'get',
        'https://gew4-spclient.spotify.com/storage-resolve/v2/files/audio/interactive/11/' +
          file + '?version=10000000&product=9&platform=39&alt=json'
      )
    ).cdnurl[0];

    if (!options.spawn) {
      return {
        decryptionKey: key,
        streamUrl,
        information: url,
      };
    }

    const child = spawn('ffmpeg', [
      '-decryption_key',
      key,
      '-i',
      streamUrl,
      '-f',
      'wav',
      '-'
    ]);
    // child.stderr.on('data', (x) => console.log(x.toString()));

    if (options.timeout !== 0) {
      const timeout = setTimeout(
        () => child.kill(9),
        options.timeout
      );
      child.on('exit', () => clearTimeout(timeout));
    }

    return {
      stream: child.stdout,
      information: url
    };
  }

  /**
   * get information about the track.
   * @param trackId the Spotify track's id.
   * @returns a Promise, which may resolve to a SpotifyTrackExtended
   * object.
   */
  public getTrack(trackId: string): Promise<SpotifyTrackExtended> {
    return this.fetchWrapper<SpotifyTrackExtended>(
      'get',
      'https://api.spotify.com/v1/tracks/' + trackId
    );
  }

  /**
   * get information about the album/EP/compilation.
   * @param albumId the Spotify album's id.
   * @returns a Promise, which may resolve to a SpotifyAlbumExtended
   * object.
   */
  public async getAlbum(albumId: string): Promise<SpotifyAlbumExtended> {
    const album = await this.fetchWrapper<SpotifyAlbumExtended>(
      'get',
      'https://api.spotify.com/v1/albums/' + albumId
    );
    let albumNextURL = album.tracks.next;

    while (albumNextURL) {
      const next = await this.fetchWrapper<SpotifyAlbumExtended>('get', albumNextURL);
      album.tracks.items = album.tracks.items.concat(next.tracks.items);
      albumNextURL = next.tracks.next;
    }

    return album;
  }

  /**
   * get information about the playlist
   * @param playlistId the Spotify playlist's id.
   * @returns a Promise, which may resolve to a SpotifyAlbumExtended
   * object.
   */
  public async getPlaylist(playlistId: string): Promise<SpotifyPlaylist> {
    const playlist = await this.fetchWrapper<SpotifyPlaylist>(
      'get',
      'https://api.spotify.com/v1/playlists/' + playlistId
    );
    let playlistNextURL = playlist.tracks.next;

    while (playlistNextURL) {
      const next = await this.fetchWrapper<SpotifyPlaylist>('get', playlistNextURL);
      playlist.tracks.items = playlist.tracks.items.concat(next.tracks.items);
      playlistNextURL = next.tracks.next;
    }

    return playlist;
  }

  /**
   * get lyrics from the track.
   * @param trackId a Spotify track's id.
   * @returns a Promise, which resolves to a string if lyrics
   * exist, or to null otherwise.
   */
  public async getLyrics(trackId: string): Promise<string | null> {
    try {
      const raw = (await this.fetchWrapper(
        'get',
        `https://spclient.wg.spotify.com/metadata/4/track/${trackId}?market=from_token`)
      ).lyrics;

      return raw.lines.map((x: { words: string }) => x.words).join('\n');
    } catch (err) {
      return null;
    }
  }
}