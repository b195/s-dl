// i was bored
import { Field, Type, Message, OneOf } from 'protobufjs/light';

export enum ClientIdentificationTokenType {
  KEYBOX = 0,
  DRM_DEVICE_CERTIFICATE = 1,
  REMOTE_ATTESTATION_CERTIFICATE = 2,
  OEM_DEVICE_CERTIFICATE = 3
};

@Type.d('NameValue')
export class ClientIdentificationNameValue extends Message<ClientIdentificationNameValue> {
  @Field.d(1, 'string', 'optional')
  public name!: string;

  @Field.d(2, 'string', 'optional')
  public value!: string;
};

export enum ClientIdentificationClientCapabilitiesHdcpVersion {
  HDCP_NONE = 0,
  HDCP_V1 = 1,
  HDCP_V2 = 2,
  HDCP_V2_1 = 3,
  HDCP_V2_2 = 4,
  HDCP_V2_3 = 5,
  HDCP_NO_DIGITAL_OUTPUT = 0xff
};

export enum ClientIdentificationClientCapabilitiesCertificateKeyType {
  RSA_2048 = 0,
  RSA_3072 = 1,
  ECC_SECP256R1 = 2,
  ECC_SECP384R1 = 3,
  ECC_SECP521R1 = 4
}

export enum ClientIdentificationClientCapabilitiesAnalogOutputCapabilities {
  ANALOG_OUTPUT_UNKNOWN = 0,
  ANALOG_OUTPUT_NONE = 1,
  ANALOG_OUTPUT_SUPPORTED = 2,
  ANALOG_OUTPUT_SUPPORTS_CGMS_A = 3
}

@Type.d('ClientCapabilities')
export class ClientIdentificationClientCapabilities extends Message<ClientIdentificationClientCapabilities> {
  @Field.d(1, 'bool', 'optional', false)
  public clientToken!: boolean;

  @Field.d(2, 'bool', 'optional', false)
  public sessionToken!: boolean;

  @Field.d(3, 'bool', 'optional', false)
  public videoResolutionConstraints!: boolean;

  @Field.d(4, ClientIdentificationClientCapabilitiesHdcpVersion, 'optional', ClientIdentificationClientCapabilitiesHdcpVersion.HDCP_NONE)
  public maxHdcpVersion!: ClientIdentificationClientCapabilitiesHdcpVersion;

  @Field.d(5, 'uint32', 'optional')
  public oemCryptoApiVersion!: number;

  @Field.d(6, 'bool', 'optional', false)
  public antiRollbackUsageTable!: boolean;

  @Field.d(7, 'uint32', 'optional')
  public srmVersion!: number;

  @Field.d(8, 'bool', 'optional', false)
  public canUpdateSrm!: boolean;

  @Field.d(9, ClientIdentificationClientCapabilitiesCertificateKeyType, 'repeated')
  public supportedCertificateKeyType!: ClientIdentificationClientCapabilitiesCertificateKeyType;

  @Field.d(10, ClientIdentificationClientCapabilitiesAnalogOutputCapabilities, 'optional', ClientIdentificationClientCapabilitiesAnalogOutputCapabilities.ANALOG_OUTPUT_UNKNOWN)
  public analogOutputCapabilities!: ClientIdentificationClientCapabilitiesAnalogOutputCapabilities;

  @Field.d(11, 'bool', 'optional', false)
  public canDisableAnalogOutput!: boolean;

  @Field.d(12, 'uint32', 'optional', 0)
  public resourceRatingTier!: number;
};

@Type.d('ClientCredentials')
export class ClientIdentificationClientCredentials extends Message<ClientIdentificationClientCredentials> {
  @Field.d(1, ClientIdentificationTokenType, 'optional')
  public type!: ClientIdentificationTokenType;

  @Field.d(2, 'bytes', 'optional')
  public token!: Buffer;
}

export class ClientIdentification extends Message<ClientIdentification> {
  @Field.d(1, ClientIdentificationTokenType, 'optional', ClientIdentificationTokenType.KEYBOX)
  public type!: ClientIdentificationTokenType;

  @Field.d(2, 'bytes', 'optional')
  public token!: Buffer;

  @Field.d(3, ClientIdentificationNameValue, 'repeated')
  public clientInfo!: ClientIdentificationNameValue[];

  @Field.d(4, 'bytes', 'optional')
  public providerClientToken!: Buffer;

  @Field.d(5, 'uint32', 'optional')
  public licenseCounter!: number;

  @Field.d(6, ClientIdentificationClientCapabilities, 'optional')
  public clientCapabilities!: ClientIdentificationClientCapabilities;

  @Field.d(7, 'bytes', 'optional')
  public vmpData!: Buffer;

  @Field.d(8, ClientIdentificationClientCredentials, 'repeated')
  public deviceCredentials!: ClientIdentificationClientCredentials;
};

export enum LicenseType {
  STREAMING = 1,
  OFFLINE = 2,
  AUTOMATIC = 3
};

@Type.d('WidevinePsshData')
export class LicenseRequestContentIdentificationWidevinePsshData extends Message<LicenseRequestContentIdentificationWidevinePsshData> {
  @Field.d(1, 'bytes', 'repeated')
  public psshData!: Buffer[];

  @Field.d(2, LicenseType, 'optional')
  public licenseType!: LicenseType;

  @Field.d(3, 'bytes', 'optional')
  public requestId!: Buffer;
};

@Type.d('WebmKeyId')
export class LicenseRequestContentIdentificationWebmKeyId extends Message<LicenseRequestContentIdentificationWebmKeyId> {
  @Field.d(1, 'bytes', 'optional')
  public header!: Buffer;

  @Field.d(2, LicenseType, 'optional')
  public licenseType!: LicenseType;

  @Field.d(3, 'bytes', 'optional')
  public requestId!: Buffer;
};

export class LicenseIdentification extends Message<LicenseIdentification> {
  @Field.d(1, 'bytes', 'optional')
  public requestId!: Buffer;

  @Field.d(2, 'bytes', 'optional')
  public sessionid!: Buffer;

  @Field.d(3, 'bytes', 'optional')
  public purchaseId!: Buffer;

  @Field.d(4, LicenseType, 'optional')
  public type!: LicenseType;

  @Field.d(5, 'int32', 'optional')
  public version!: number;

  @Field.d(6, 'bytes', 'optional')
  public providerSessionToken!: Buffer;
}

@Type.d('ExistingLicense')
export class LicenseRequestContentIdentificationExistingLicense extends Message<LicenseRequestContentIdentificationWebmKeyId> {
  @Field.d(1, LicenseIdentification, 'optional')
  public licenseId!: LicenseIdentification;

  @Field.d(2, 'int64', 'optional')
  public secondsSinceStarted!: number;

  @Field.d(3, 'int64', 'optional')
  public secondsSinceLastPlayed!: number;

  @Field.d(4, 'bytes', 'optional')
  public sessionUsageTableEntry!: Buffer;
};

export enum LicenseRequestContentIdentificationInitDataInitDataType {
  CENC = 1,
  WEBM = 2
};

export class LicenseRequestContentIdentificationInitData extends Message<LicenseRequestContentIdentificationInitData> {
  @Field.d(1, LicenseRequestContentIdentificationInitDataInitDataType, 'optional', LicenseRequestContentIdentificationInitDataInitDataType.CENC)
  public initDataType!: LicenseRequestContentIdentificationInitDataInitDataType;

  @Field.d(2, 'bytes', 'optional')
  public initData!: Buffer;

  @Field.d(3, LicenseType, 'optional')
  public licenseType!: LicenseType;

  @Field.d(4, 'bytes', 'optional')
  public requestId!: Buffer;
};

@Type.d('ContentIdentification')
export class LicenseRequestContentIdentification extends Message<LicenseRequestContentIdentification> {
  @Field.d(1, LicenseRequestContentIdentificationWidevinePsshData)
  public widevinePsshData!: LicenseRequestContentIdentificationWidevinePsshData;

  @Field.d(2, LicenseRequestContentIdentificationWebmKeyId)
  public webmKeyId!: LicenseRequestContentIdentificationWebmKeyId;

  @Field.d(3, LicenseRequestContentIdentificationExistingLicense)
  public existingLicense!: LicenseRequestContentIdentificationExistingLicense;

  @Field.d(4, LicenseRequestContentIdentificationInitData)
  public initData!: LicenseRequestContentIdentificationInitData;

  @OneOf.d('widevinePsshData', 'webmKeyId', 'existingLicense', 'initData')
  public contentIdVariant!: string;
};

export enum LicenseRequestRequestType {
  NEW = 1,
  RENEWAL = 2,
  RELEASE = 3
};

export enum ProtocolVersion {
  VERSION_2_0 = 20,
  VERSION_2_1 = 21,
  VERSION_2_2 = 22
};

export class EncryptedClientIdentification extends Message<EncryptedClientIdentification> {
  @Field.d(1, 'string', 'optional')
  public providerId!: string;

  @Field.d(2, 'bytes', 'optional')
  public serviceCertificateSerialNumber!: Buffer;

  @Field.d(3, 'bytes', 'optional')
  public encryptedClientId!: Buffer;

  @Field.d(4, 'bytes', 'optional')
  public encryptedClientIdIv!: Buffer;

  @Field.d(5, 'bytes', 'optional')
  public encryptedPrivacyKey!: Buffer;
}

export class LicenseRequest extends Message<LicenseRequest> {
  @Field.d(1, ClientIdentification, 'optional')
  public clientId!: ClientIdentification;

  @Field.d(2, LicenseRequestContentIdentification, 'optional')
  public contentId!: LicenseRequestContentIdentification;

  @Field.d(3, LicenseRequestRequestType, 'optional')
  public type!: LicenseRequestRequestType;

  @Field.d(4, 'int64', 'optional')
  public requestTime!: number;

  @Field.d(5, 'bytes', 'optional')
  public keyControlNonceDeprecated!: Buffer;

  @Field.d(6, ProtocolVersion, 'optional', ProtocolVersion.VERSION_2_0)
  public protocolVersion!: ProtocolVersion;

  @Field.d(7, LicenseRequestRequestType, 'optional')
  public keyControlNonce!: number;

  @Field.d(8, LicenseRequestRequestType, 'optional')
  public encryptedClientId!: EncryptedClientIdentification; // i am NOT doing a type for this
};

export enum SignedMessageMessageType {
  LICENSE_REQUEST = 1,
  LICENSE = 2,
  ERROR_RESPONSE = 3,
  SERVICE_CERTIFICATE_REQUEST = 4,
  SERVICE_CERTIFICATE = 5,
  SUB_LICENSE = 6,
  CAS_LICENSE_REQUEST = 7,
  CAS_LICENSE = 8,
  EXTERNAL_LICENSE_REQUEST = 9,
  EXTERNAL_LICENSE = 10
};

export enum SignedMessageSessionKeyType {
  UNDEFINED = 0,
  WRAPPED_AES_KEY = 1,
  EPHERMERAL_ECC_PUBLIC_KEY = 2
};

export enum MetricDataMetricType {
  LATENCY = 1,
  TIMESTAMP = 2
};

@Type.d('TypeValue')
export class MetricDataTypeValue extends Message<MetricDataTypeValue> {
  @Field.d(1, MetricDataMetricType, 'optional')
  public type!: MetricDataMetricType;

  @Field.d(2, 'int64', 'optional', 0)
  public value!: number;
};

export class MetricData extends Message<MetricData> {
  @Field.d(1, 'string', 'optional')
  public stageName!: string;

  @Field.d(2, MetricDataTypeValue, 'repeated')
  public metricData!: MetricDataTypeValue[];
};

export class VersionInfo extends Message<VersionInfo> {
  @Field.d(1, 'string', 'optional')
  public licenseSdkVersion!: string;

  @Field.d(2, 'string', 'optional')
  public licenseServiceVersion!: string;
};

export class SignedMessage extends Message<SignedMessage> {
  @Field.d(1, SignedMessageMessageType, 'optional')
  public type!: SignedMessageMessageType;

  @Field.d(2, 'bytes', 'optional')
  public msg!: Buffer;

  @Field.d(3, 'bytes', 'optional')
  public signature!: Buffer;

  @Field.d(4, 'bytes', 'optional')
  public sessionKey!: Buffer;

  @Field.d(5, 'bytes', 'optional')
  public remoteAttestation!: Buffer;

  @Field.d(6, MetricData, 'repeated')
  public metricData!: MetricData[];

  @Field.d(7, VersionInfo, 'optional')
  public serviceVersionInfo!: VersionInfo;

  @Field.d(8, SignedMessageSessionKeyType, 'optional', SignedMessageSessionKeyType.WRAPPED_AES_KEY)
  public sessionKeyType!: SignedMessageSessionKeyType;

  @Field.d(9, 'bytes', 'optional')
  public oemcryptoCoreMessage!: Buffer;
};

@Type.d('Policy')
export class LicensePolicy extends Message<LicensePolicy> {
  @Field.d(1, 'bool', 'optional', false)
  public canPlay!: boolean;

  @Field.d(2, 'bool', 'optional', false)
  public canPersist!: boolean;

  @Field.d(3, 'bool', 'optional', false)
  public canRenew!: boolean;

  @Field.d(4, 'int64', 'optional', 0)
  public rentalDurationSeconds!: number;

  @Field.d(5, 'int64', 'optional', 0)
  public playbackDurationSeconds!: number;

  @Field.d(6, 'int64', 'optional', 0)
  public licenseDurationSeconds!: number;

  @Field.d(7, 'int64', 'optional', 0)
  public renewalRecoveryDurationSeconds!: number;

  @Field.d(8, 'string', 'optional')
  public renewalServerUrl!: string;

  @Field.d(9, 'int64', 'optional', 0)
  public renewalDelaySeconds!: string;

  @Field.d(10, 'int64', 'optional', 0)
  public renewalRetryIntervalSeconds!: number;

  @Field.d(11, 'bool', 'optional', false)
  public renewWithUsage!: boolean;

  @Field.d(12, 'bool', 'optional', false)
  public alwaysIncludeClientId!: boolean;

  @Field.d(13, 'int64', 'optional', 0)
  public okayStartGracePeriodSeconds!: number;

  @Field.d(14, 'bool', 'optional', false)
  public softEnforcePlaybackDuration!: boolean;

  @Field.d(15, 'bool', 'optional', true)
  public softEnforceRentalDuration!: boolean;
};

export enum LicenseKeyContainerKeyType {
  SIGNING = 1,
  CONTENT = 2,
  KEY_CONTROL = 3,
  OPERATOR_SESSION = 4,
  ENTITLEMENT = 5,
  OEM_CONTENT = 6
};

export enum LicenseKeyContainerSecurityLevel {
  SW_SECURE_CRYPTO = 1,
  SW_SECURE_DECODE = 2,
  HW_SECURE_CRYPTO = 3,
  HW_SECURE_DECODE = 4,
  HW_SECURE_ALL = 5
};

@Type.d('KeyControl')
export class LicenseKeyContainerKeyControl extends Message<LicenseKeyContainerKeyControl> {
  @Field.d(1, 'bytes', 'optional')
  public keyControlBlock!: Buffer;

  @Field.d(2, 'bytes', 'optional')
  public iv!: Buffer;
};

export enum LicenseKeyContainerOutputProtectionCGMS {
  CGMS_NONE = 42,
  COPY_FREE = 0,
  COPY_ONCE = 2,
  COPY_NEVER = 3,
};

export enum LicenseKeyContainerOutputProtectionHDCP {
  HDCP_NONE = 0,
  HDCP_V1 = 1,
  HDCP_V2 = 2,
  HDCP_V2_1 = 3,
  HDCP_V2_2 = 4,
  HDCP_V2_3 = 5,
  HDCP_NO_DIGITAL_OUTPUT = 0xff
};

export enum LicenseKeyContainerOutputProtectionHdcpSrmRule {
  HDCP_SRM_RULE_NONE = 0,
  CURRENT_SRM = 1
};

@Type.d('OutputProtection')
export class LicenseKeyContainerOutputProtection extends Message<LicenseKeyContainerOutputProtection> {
  @Field.d(1, LicenseKeyContainerOutputProtectionHDCP, 'optional', LicenseKeyContainerOutputProtectionHDCP.HDCP_NONE)
  public hdcp!: LicenseKeyContainerOutputProtectionHDCP;

  @Field.d(2, LicenseKeyContainerOutputProtectionCGMS, 'optional', LicenseKeyContainerOutputProtectionCGMS.CGMS_NONE)
  public cgmsFlags!: LicenseKeyContainerOutputProtectionCGMS;

  @Field.d(3, LicenseKeyContainerOutputProtectionHdcpSrmRule, 'optional', LicenseKeyContainerOutputProtectionHdcpSrmRule.HDCP_SRM_RULE_NONE)
  public hdcpSrmRule!: LicenseKeyContainerOutputProtectionHdcpSrmRule;

  @Field.d(4, 'bool', 'optional', false)
  public disableAnalogOutput!: boolean;

  @Field.d(5, 'bool', 'optional', false)
  public disableDigitalOutput!: boolean;
};

@Type.d('VideoResolutionConstraint')
export class LicenseKeyContainerVideoResolutionConstraint extends Message<LicenseKeyContainerVideoResolutionConstraint> {
  @Field.d(1, 'uint32', 'optional')
  public minResolutionPixels!: number;

  @Field.d(2, 'uint32', 'optional')
  public maxResolutionPixels!: number;

  @Field.d(3, LicenseKeyContainerOutputProtection, 'optional')
  public requiredProtection!: LicenseKeyContainerOutputProtection;
};

@Type.d('OperatorSessionKeyPermissions')
export class LicenseKeyContainerOperatorSessionKeyPermissions extends Message<LicenseKeyContainerOperatorSessionKeyPermissions> {
  @Field.d(1, 'bool', 'optional', false)
  public allowEncrypt!: boolean;

  @Field.d(2, 'bool', 'optional', false)
  public allowDecrypt!: boolean;

  @Field.d(3, 'bool', 'optional', false)
  public allowSign!: boolean;

  @Field.d(4, 'bool', 'optional', false)
  public allowSignatureVerify!: boolean;
};

@Type.d('KeyContainer')
export class LicenseKeyContainer extends Message<LicenseKeyContainer> {
  @Field.d(1, 'bytes', 'optional')
  public id!: Buffer;

  @Field.d(2, 'bytes', 'optional')
  public iv!: Buffer;

  @Field.d(3, 'bytes', 'optional')
  public key!: Buffer;

  @Field.d(4, LicenseKeyContainerKeyType, 'optional')
  public type!: LicenseKeyContainerKeyType;

  @Field.d(5, LicenseKeyContainerSecurityLevel, 'optional', LicenseKeyContainerSecurityLevel.SW_SECURE_CRYPTO)
  public level!: LicenseKeyContainerSecurityLevel;

  @Field.d(6, LicenseKeyContainerOutputProtection, 'optional')
  public requiredProtection!: LicenseKeyContainerOutputProtection;

  @Field.d(7, LicenseKeyContainerOutputProtection, 'optional')
  public requestedProtection!: LicenseKeyContainerOutputProtection;

  @Field.d(8, LicenseKeyContainerKeyControl, 'optional')
  public keyControl!: LicenseKeyContainerKeyControl;

  @Field.d(9, LicenseKeyContainerOperatorSessionKeyPermissions, 'optional')
  public operatorSessionKeyPermissions!: LicenseKeyContainerOperatorSessionKeyPermissions;

  @Field.d(10, LicenseKeyContainerVideoResolutionConstraint, 'repeated')
  public videoResolutionConstraints!: LicenseKeyContainerVideoResolutionConstraint[];

  @Field.d(11, 'bool', 'optional', false)
  public antiRollbackUsageTable!: boolean;

  @Field.d(12, 'string', 'optional')
  public trackLabel!: string;
};

export enum PlatformVerificationStatus {
  PLATFORM_UNVERIFIED = 0,
  PLATFORM_TAMPERED = 1,
  PLATFORM_SOFTWARE_VERIFIED = 2,
  PLATFORM_HARDWARE_VERIFIED = 3,
  PLATFORM_NO_VERIFICATION = 4,
  PLATFORM_SECURE_STORAGE_SOFTWARE_VERIFIED = 5
};

export class License extends Message<License> {
  @Field.d(1, LicenseIdentification, 'optional')
  public id!: LicenseIdentification;

  @Field.d(2, LicensePolicy, 'optional')
  public policy!: LicensePolicy;

  @Field.d(3, LicenseKeyContainer, 'repeated')
  public key!: LicenseKeyContainer[];

  @Field.d(4, 'int64', 'optional')
  public licenseStartTime!: number;

  @Field.d(5, 'bool', 'optional', false)
  public remoteAttestationVerified!: boolean;

  @Field.d(6, 'bytes', 'optional')
  public providerClientToken!: Buffer;

  @Field.d(7, 'uint32', 'optional')
  public protectionScheme!: number;

  @Field.d(8, 'bytes', 'optional')
  public srmRequirement!: Buffer;

  @Field.d(9, 'bytes', 'optional')
  public srmUpdate!: Buffer;

  @Field.d(10, PlatformVerificationStatus, 'optional', PlatformVerificationStatus.PLATFORM_NO_VERIFICATION)
  public platformVerificationStatus!: PlatformVerificationStatus;

  @Field.d(11, 'bytes', 'repeated')
  public groupIds!: Buffer[];
};