import {
  randomBytes,
  createSign,
  constants,
  createHmac,
  privateDecrypt,
  createDecipheriv
} from 'crypto';
import { aesCmac } from 'node-aes-cmac';

import {
  ClientIdentification,
  License,
  LicenseKeyContainerKeyType,
  LicenseRequest,
  LicenseRequestContentIdentification,
  LicenseRequestContentIdentificationWidevinePsshData,
  LicenseRequestRequestType,
  LicenseType,
  ProtocolVersion,
  SignedMessage,
  SignedMessageMessageType
} from './proto';

interface Context {
  encContext: Buffer;
  macContext: Buffer;
};

export default class CDM {
  private clientId: ClientIdentification;
  private privateKey: Buffer;
  private storage: Map<string, Context> = new Map;

  constructor(privateKey: Buffer, clientIdentification: Buffer) {
    this.privateKey = privateKey;
    this.clientId = ClientIdentification.decode(clientIdentification);
  }

  private deriveContext(licenseRequest: Buffer): Context {
    const bigEndian32 = (num: number) => {
      const buffer = Buffer.alloc(4);
      buffer.writeInt32BE(num);
      return buffer;
    };

    const getEncContext = (msg: Buffer) =>
      Buffer.concat([
        Buffer.from('ENCRYPTION'),
        Buffer.from([0]),
        msg,
        bigEndian32(16 * 8)
      ]);

    const getMacContext = (msg: Buffer) =>
      Buffer.concat([
        Buffer.from('AUTHENTICATION'),
        Buffer.from([0]),
        msg,
        bigEndian32(32 * 8 * 2)
      ]);

    return {
      encContext: getEncContext(licenseRequest),
      macContext: getMacContext(licenseRequest)
    };
  }

  public getLicenseChallenge(pssh: string) {
    // we're going to straight up assume that the type of the device is ANDROID.
    let requestId = Buffer.concat([ randomBytes(4), Buffer.alloc(4) ]);

    const sessionNumber = Buffer.alloc(8);
    sessionNumber.writeInt32LE(1);

    requestId = Buffer.concat([ requestId, sessionNumber ]);
    requestId = Buffer.from(requestId.toString('hex').toUpperCase());

    const licenseRequest = new LicenseRequest({
      type: LicenseRequestRequestType.NEW,
      requestTime: Math.floor(Date.now() / 1000),
      protocolVersion: ProtocolVersion.VERSION_2_1,
      keyControlNonce: 1 + Math.floor(Math.random() * (2 ** 31 - 1)),
      clientId: this.clientId,
      contentId: new LicenseRequestContentIdentification({
        widevinePsshData: new LicenseRequestContentIdentificationWidevinePsshData({
          // skip 32 bytes of pssh stuff
          psshData: [Buffer.from(pssh, 'base64').subarray(32)],
          licenseType: LicenseType.STREAMING,
          requestId: requestId
        })
      })
    });

    const msg = Buffer.from(LicenseRequest.encode(licenseRequest).finish());
    const signer = createSign('RSA-SHA1');
    signer.update(msg);
    const signature = signer.sign({
      key: this.privateKey,
      padding: constants.RSA_PKCS1_PSS_PADDING,
      saltLength: constants.RSA_PSS_SALTLEN_DIGEST
    });

    const licenseMessage = new SignedMessage({
      type: SignedMessageMessageType.LICENSE_REQUEST,
      msg,
      signature
    });

    this.storage.set(requestId.toString('utf-8'), this.deriveContext(msg));

    return SignedMessage.encode(licenseMessage).finish();
  }

  public parseLicense(binary: Buffer) {
    const message = SignedMessage.decode(binary);
    const license = License.decode(message.msg);
    const requestId = license.id.requestId.toString('utf-8');

    if (!this.storage.has(requestId))
      throw new Error('no license request with such request id was made beforehand');
    const ctx = this.storage.get(requestId) as Context;

    const key = privateDecrypt(this.privateKey, message.sessionKey);
    const keys = this.deriveKeys(ctx, key);

    const hmac = createHmac('sha256', keys.macKeyServer);
    hmac.update(Buffer.from(message.oemcryptoCoreMessage));
    hmac.update(message.msg);
    const signature = hmac.digest();

    if (Buffer.compare(message.signature, signature) !== 0)
      throw new Error('signature mismatch!');

    // spotify-specific. we're looking for a CONTENT key.
    const contentKey = license.key.filter(x =>
      x.type == LicenseKeyContainerKeyType.CONTENT
    )[0];

    if (!contentKey)
      throw new Error('no content key available');

    const cipher = createDecipheriv('AES-128-CBC', keys.encKey, contentKey.iv);
    const decrypted = Buffer.concat([ cipher.update(contentKey.key), cipher.final() ]);

    this.storage.delete(requestId);
    return decrypted.toString('hex');
  }

  private deriveKeys(context: Context, decryptedKey: Buffer) {
    const derive = (contextData: Buffer, counter: number) =>
      aesCmac(
        decryptedKey,
        Buffer.concat([ Buffer.from([ counter ]), contextData ]),
        { returnAsBuffer: true }
      );

    const encKey = derive(context.encContext, 1);
    const macKeyServer = Buffer.concat([
      derive(context.macContext, 1),
      derive(context.macContext, 2)
    ]);
    const macKeyClient = Buffer.concat([
      derive(context.macContext, 3),
      derive(context.macContext, 4)
    ]);

    return { encKey, macKeyServer, macKeyClient };
  }


}