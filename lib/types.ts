export interface SpotifyBase {
  external_urls: Record<string, string>;
  href: string;
  id: string;
  name: any;
  type: 'album' | 'track' | 'artist' | 'playlist' | 'user' | 'episode';
  uri: string;
};

export interface SpotifyArtist extends SpotifyBase {
  type: 'artist'
};

export interface SpotifyPlayableData extends SpotifyBase {
  artists: SpotifyArtist[];
  available_markets: string[];
  type: 'album' | 'track' | 'episode';
};

export interface SpotifyImage {
  height: number;
  url: string;
  width: number;
};

export interface SpotifyAlbum extends SpotifyPlayableData {
  album_type: 'album' | 'single' | 'compilation';
  images: SpotifyImage[];
  total_tracks: number;
  release_date: string;
  release_date_precision: 'year' | 'month' | 'day';
  type: 'album';
};

export interface SpotifyBaseTrack extends SpotifyPlayableData {
  disc_number: number;
  duration_ms: number;
  explicit: boolean;
  is_local: boolean;
  preview_url: string;
  track_number: number;
  type: 'track' | 'episode';
};

export interface SpotifyTrack extends SpotifyBaseTrack {
  type: 'track';
};

export interface SpotifyEpisode extends SpotifyBaseTrack {
  type: 'episode';
};

export interface SpotifyExtendedData {
  external_ids: Record<string, string>;
  popularity: number;
};

export interface SpotifyIterableTracks<T> {
  href: string;
  items: T[];
  limit: number;
  next: string | null;
  offset: 0;
  previous: string | null;
  total: number;
};

export interface SpotifyIterable<T> {
  tracks: SpotifyIterableTracks<T>;
};

export interface SpotifyAlbumExtendedCopyright {
  text: string;
  type: 'C' | 'P';
};

export interface SpotifyUser extends SpotifyBase {
  display_name: string;
  name: undefined;
};

export interface SpotifySharingInfo {
  share_id: string;
  share_url: string;
  uri: string;
};

export interface SpotifyPlaylistData {
  primary_color: any;
  sharing_info: SpotifySharingInfo;
};

export interface SpotifyPlaylistItem extends SpotifyPlaylistData {
  added_at: string;
  added_by: SpotifyBase;
  is_local: boolean;
  track: SpotifyBaseTrackExtended;
}

export interface SpotifyPlaylist extends SpotifyBase, SpotifyIterable<SpotifyPlaylistItem>, SpotifyPlaylistData {
  collaborative: boolean;
  description: string;
  followers: { href: string | null; total: number; };
  images: SpotifyImage[];
  owner: SpotifyUser;
  primary_color: any;
  public: boolean;
  sharing_info: SpotifySharingInfo;
  snapshot_id: string;
  type: 'playlist';
};

export interface SpotifyAlbumExtended extends SpotifyAlbum, SpotifyIterable<SpotifyTrack> {
  copyrights: SpotifyAlbumExtendedCopyright[];
  genres: string[];
  label: string;
};

export interface SpotifyBaseTrackExtended extends SpotifyBaseTrack, SpotifyExtendedData {
  album: SpotifyAlbum;
};

export interface SpotifyTrackExtended extends SpotifyBaseTrackExtended {
  type: 'track';
};

export interface SpotifyEpisodeExtended extends SpotifyBaseTrackExtended {
  type: 'episode';
};